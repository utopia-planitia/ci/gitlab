apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/proxy-body-size: 1G
  name: gitlab
spec:
  rules:
    - host: "{{ .Values.domain }}"
      http:
        paths:
          - backend:
              service:
                name: gitlab
                port:
                  number: 80
            path: /
            pathType: ImplementationSpecific
  tls:
    - hosts:
        - "{{ .Values.domain }}"
      secretName: "{{ .Values.tls_wildcard_secret }}"
